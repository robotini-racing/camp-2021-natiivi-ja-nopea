FROM ubuntu

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get -yq install llvm build-essential libopencv-dev clang libclang-dev curl
RUN curl https://sh.rustup.rs -sSf | bash -s -- -y

RUN mkdir -p /opt/app/
WORKDIR /opt/app

COPY ./Cargo.lock .
COPY ./Cargo.toml .
RUN mkdir src && touch src/main.rs && echo 'fn main() {}' > src/main.rs
RUN ~/.cargo/bin/cargo check --release

COPY ./src ./src

RUN ~/.cargo/bin/cargo install --path .

CMD ["/root/.cargo/bin/robotini-rs"]
