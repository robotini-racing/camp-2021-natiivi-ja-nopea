use std::{
    collections::VecDeque,
    io::{stdout, Stdout},
};

use opencv::{
    core::{
        bitwise_and, bitwise_or, bitwise_xor, count_non_zero, no_array, normalize, split, subtract,
        Point_, Rect_, Scalar_, Size, BORDER_CONSTANT, CV_8UC3, NORM_MINMAX,
    },
    highgui, imgcodecs,
    imgproc::{
        cvt_color, dilate, erode, get_structuring_element, morphology_default_border_value,
        threshold, COLOR_BGR2GRAY, LINE_8, MORPH_RECT, THRESH_BINARY, THRESH_OTSU, THRESH_TRUNC,
    },
    photo::illumination_change,
    prelude::*,
    types::{VectorOfMat, VectorOfu8},
};

mod connection;
use connection::{Command, Connection, LoginMessage};
use linreg::linear_regression;
use termion::screen::AlternateScreen;
use tui::{
    backend::TermionBackend,
    layout::{Constraint, Direction, Layout},
    style::{Color, Modifier, Style},
    symbols,
    text::Span,
    widgets::{Axis, Block, Borders, Chart, Dataset, Gauge, List, ListItem, Sparkline},
    Terminal,
};

struct Config {
    pub save_images: bool,
    pub log_verbose: bool,
    pub gui: bool,
    pub tui: bool,
    pub team_id: String,
    pub address: String,
}

impl Config {
    fn from_env() -> Config {
        Config {
            save_images: !std::env::var("DEBUG_IMAGES")
                .unwrap_or(String::from(""))
                .is_empty(),
            log_verbose: !std::env::var("DEBUG_VERBOSE")
                .unwrap_or(String::from(""))
                .is_empty(),
            gui: !std::env::var("DEBUG_GUI")
                .unwrap_or(String::from(""))
                .is_empty(),
            tui: !std::env::var("DEBUG_TUI")
                .unwrap_or(String::from(""))
                .is_empty(),
            team_id: std::env::var("teamid").unwrap_or(String::from("rust")),
            address: std::env::var("SIMULATOR").unwrap_or(String::from("127.0.0.1:11000")),
        }
    }
}

fn save_frame(config: &Config, mat: &Mat, i: usize) -> anyhow::Result<()> {
    let image_name = format!("captures/frame{:04}.png", i);
    save_frame_to_file(config, &image_name, mat)
}

fn save_frame_to_file(config: &Config, name: &str, mat: &Mat) -> anyhow::Result<()> {
    if !config.save_images {
        return Ok(());
    }

    imgcodecs::imwrite(&name, &mat, &opencv::core::Vector::<i32>::new())?;
    Ok(())
}

fn run() -> anyhow::Result<()> {
    std::fs::create_dir_all("captures/debug")?;

    let config = Config::from_env();

    let mut connection = Connection::connect(
        &config.address,
        &LoginMessage {
            name: "Natiivi ja Nopea",
            color: "#ff9514",
            team_id: &config.team_id,
            texture: "https://stuff.h4x0rb34.rs/robotini.png",
        },
    )?;

    if config.gui {
        highgui::named_window("robotini B", 1)?;
        // highgui::named_window("robotini G", 1)?;
        // highgui::named_window("robotini R", 1)?;
    }

    let mut frame_i = 0;
    let mut car_state = CarState {
        speed: 0.0,
        wheels_turn: 0.0,
        previous_horizons: VecDeque::new(),
        pixel_speed: 0.0,
        previous_blue_count: 0.0,
        previous_red_count: 0.0,
        previous_green_count: 0.0,
        command_history: VecDeque::new(),
        previous_center: Some(0.0),
        previous_range: None,
        previous_centers: VecDeque::new(),
    };

    let mut terminal = if config.tui {
        let stdout = AlternateScreen::from(stdout());
        let backend = TermionBackend::new(stdout);
        Some(Terminal::new(backend)?)
    } else {
        None
    };

    loop {
        let image = connection.read_next_image()?;

        let frame =
            opencv::imgcodecs::imdecode(&VectorOfu8::from(image), opencv::imgcodecs::IMREAD_COLOR)?;

        save_frame(&config, &frame, frame_i)?;
        frame_update(&config, &frame, &mut car_state, &mut &mut connection)?;

        if config.tui {
            if let (Some(mut terminal), true) = (terminal.as_mut(), frame_i % 10 == 0) {
                render_debug_tui(&mut terminal, &car_state);
            }
        }

        if config.gui {
            let key = highgui::wait_key(10)?;
            if key > 0 && key != 255 {
                break;
            }
        }

        frame_i += 1;
    }
    Ok(())
}

#[derive(Debug)]
struct CarState {
    wheels_turn: f32,
    speed: f32,
    previous_horizons: VecDeque<i32>,
    pixel_speed: f32,
    previous_blue_count: f32,
    previous_red_count: f32,
    previous_green_count: f32,
    command_history: VecDeque<String>,
    previous_center: Option<f32>,
    previous_range: Option<(usize, usize)>,
    previous_centers: VecDeque<f32>,
}

fn render_debug_tui(
    terminal: &mut Terminal<TermionBackend<AlternateScreen<Stdout>>>,
    car_state: &CarState,
) {
    terminal
        .draw(|f| {
            let chunks = Layout::default()
                .direction(Direction::Vertical)
                .margin(2)
                .constraints([Constraint::Length(40), Constraint::Min(10)].as_ref())
                .split(f.size());

            let horizon_sparkline_data = car_state
                .previous_horizons
                .iter()
                .rev()
                .map(|horizon| 80 - *horizon as u64)
                .collect::<Vec<_>>();

            let horizon_sparkline = Sparkline::default()
                .block(Block::default().title("Horizon").borders(Borders::ALL))
                .data(&horizon_sparkline_data)
                .max(80);
            f.render_widget(horizon_sparkline, chunks[0]);

            let command_log = car_state
                .command_history
                .iter()
                .map(|command| ListItem::new(command.clone()))
                .collect::<Vec<_>>();
            let command_log_list = List::new(command_log);
            f.render_widget(command_log_list, chunks[1]);
        })
        .unwrap();
}

fn frame_update(
    config: &Config,
    frame: &Mat,
    state: &mut CarState,
    connection: &mut Connection,
) -> anyhow::Result<()> {
    let wheels_turn = &mut state.wheels_turn;
    let speed = &mut state.speed;

    let (track_mask, blue, green, red) = process_frame(&config, frame)?;

    let (horizon_i, _horizon_blackness) = {
        let filtered = &track_mask;
        let cols = filtered.cols();

        (0..filtered.rows() / 2)
            .rev()
            .map(|y| {
                let row = filtered.at_row::<u8>(y).unwrap();
                let black_pixel_count = row.iter().filter(|px| **px == 0).count();
                let fullness = black_pixel_count as f32 / cols as f32;
                (y, fullness)
            })
            .max_by(|(_, fullness_a), (_, fullness_b)| {
                PartialOrd::partial_cmp(fullness_a, fullness_b).unwrap()
            })
    }
    .unwrap();

    state.previous_horizons.push_front(horizon_i);
    state.previous_horizons.truncate(60);
    let horizon_interpolated = (state.previous_horizons.iter().sum::<i32>() as f32
        / state.previous_horizons.len() as f32) as i32;

    let width = red.cols();
    let height = red.rows();

    let mut range = None;
    let mut blue_found_row = 30;

    for y in blue_found_row..height {
        let row = track_mask.at_row::<u8>(y).unwrap();

        let mut first_left = None;
        let mut first_right = None;

        for (x, value) in row.iter().enumerate() {
            if *value > 0 {
                first_left = Some(x);
                break;
            }
        }

        for (x, value) in row.iter().enumerate().rev() {
            if *value > 0 {
                first_right = Some(x);
                break;
            }
        }

        match (first_left, first_right) {
            (Some(l), Some(r)) if (r - l) > 30 => {
                blue_found_row = y;
                range = Some((l, r));
                break;
            }
            _ => {}
        };
    }

    let center = match range {
        None => None,
        Some((l, r)) => {
            let length = r as f32 - l as f32;
            Some(l as f32 + length / 2.0)
        }
    };

    let center_roi_rect = Rect_ {
        x: width / 2 - 10,
        y: 30,
        width: 20,
        height: 10,
    };

    let front_red = Mat::roi(&red, center_roi_rect)?;
    let front_red_count = count_non_zero(&front_red)?;
    let front_green = Mat::roi(&green, center_roi_rect)?;
    let front_green_cont = count_non_zero(&front_green)?;

    state
        .command_history
        .push_front(format!("Front red: {:.4}", front_red_count));

    state
        .command_history
        .push_front(format!("Front green: {:.4}", front_green_cont));

    match center {
        Some(center) => {
            state.previous_centers.push_front(center);
            state.previous_centers.truncate(2);
        }
        None => {}
    }

    let center_lolol =
        state.previous_centers.iter().sum::<f32>() / state.previous_centers.len() as f32;

    //  state.previous_centers.push_front();

    let screen_center = width as f32 / 2.0;
    let mut diff = (screen_center - center_lolol) / screen_center;
    // let diff = if diff.abs() < 0.05 { 0.0 } else { diff };

    let range_loolol = range.unwrap_or((0, width as usize));
    let range_len = (range_loolol.1 as f32 - range_loolol.0 as f32) / width as f32;

    let red_boost = ((front_red_count as f32 / 100.0) * 0.25).max(0.6);
    let red_boost = if red_boost < 0.1 { 0.0 } else { red_boost };

    let green_boost = ((front_green_cont as f32 / 100.0) * 0.25).max(0.6);
    let green_boost = if green_boost < 0.1 { 0.0 } else { green_boost };

    diff -= red_boost;
    diff += green_boost;

    *wheels_turn = (diff * diff.abs() * 1.0).max(-1.0f32).min(1.0f32);
    let min_speed = 0.002;

    let max_speed = (horizon_interpolated as f32 / 15.0) * 0.1 + 0.075;

    *speed = (0.1 / wheels_turn.abs().max(0.05))
        .min(max_speed)
        .max(min_speed);

    if config.tui {
        state.command_history.push_front(format!(
            "red_boost: {:.4}  green_boost: {:.4}",
            red_boost, green_boost
        ));
        state.command_history.truncate(30);
    }

    connection.send(&Command::Forward { value: *speed })?;
    connection.send(&Command::Turn {
        value: *wheels_turn,
    })?;

    // *wheels_turn *= 0.3;

    let mut viz_frame = red.clone();

    if config.gui {
        if let Some(range) = state.previous_range {
            let (range_start, range_end) = range;

            opencv::imgproc::line(
                &mut viz_frame,
                Point_ {
                    x: range_start as i32,
                    y: blue_found_row,
                },
                Point_ {
                    x: range_end as i32,
                    y: blue_found_row,
                },
                Scalar_([0.0, 0.0, 255.0, 0.5]),
                2,
                LINE_8,
                0,
            )?;
        }

        if let Some(center) = center {
            let (range_start, range_end) = range.unwrap();

            opencv::imgproc::line(
                &mut viz_frame,
                Point_ {
                    x: center as i32,
                    y: 0,
                },
                Point_ {
                    x: center as i32,
                    y: 80,
                },
                Scalar_([1.0, 0.0, 1.0, 1.0]),
                2,
                LINE_8,
                0,
            )?;

            opencv::imgproc::line(
                &mut viz_frame,
                Point_ {
                    x: range_start as i32,
                    y: blue_found_row,
                },
                Point_ {
                    x: range_end as i32,
                    y: blue_found_row,
                },
                Scalar_([50.0, 50.0, 50.0, 0.5]),
                2,
                LINE_8,
                0,
            )?;
        }

        highgui::imshow("robotini B", &viz_frame)?;
        // highgui::imshow("robotini G", &green_roi)?;
        // highgui::imshow("robotini R", &red_roi)?;
    }

    state.previous_center = center;
    state.previous_range = range;

    Ok(())
}

fn process_frame(config: &Config, frame: &Mat) -> anyhow::Result<(Mat, Mat, Mat, Mat)> {
    // calculate the blacks
    let mut gray_scaled = frame.clone();
    cvt_color(&frame, &mut gray_scaled, COLOR_BGR2GRAY, 0)?;
    save_frame_to_file(&config, "captures/debug/graycolor.png", &gray_scaled)?;

    let mut th = frame.clone();
    threshold(&mut gray_scaled, &mut th, 100.0, 255.0, THRESH_BINARY)?;

    let mut blacks = frame.clone();
    erode(
        &th,
        &mut blacks,
        &get_structuring_element(
            MORPH_RECT,
            Size {
                width: 4,
                height: 4,
            },
            Point_ { x: -1, y: -1 },
        )
        .ok()
        .unwrap(),
        Point_ { x: -1, y: -1 },
        1,
        BORDER_CONSTANT,
        morphology_default_border_value().ok().unwrap(),
    )?;

    let mask = Mat::default();
    let mut normalized = frame.clone();
    normalize(
        &frame,
        &mut normalized,
        0.0,
        255.0,
        NORM_MINMAX,
        -1,
        &mask.ok().unwrap(),
    )?;

    let mut preprosessed_image = frame.clone();
    bitwise_and(&normalized, &normalized, &mut preprosessed_image, &blacks).ok();

    let mut split_frame = VectorOfMat::new();
    split_frame.push(Mat::default()?);
    split_frame.push(Mat::default()?);
    split_frame.push(Mat::default()?);
    split(&preprosessed_image, &mut split_frame)?;

    // erode the blue green and red

    fn process_channel(buffer: &Mat, channel_threshold: f64) -> Mat {
        let mut eroded = buffer.clone();
        erode(
            &buffer,
            &mut eroded,
            &get_structuring_element(
                MORPH_RECT,
                Size {
                    width: 2,
                    height: 2,
                },
                Point_ { x: -1, y: -1 },
            )
            .unwrap(),
            Point_ { x: -1, y: -1 },
            1,
            BORDER_CONSTANT,
            morphology_default_border_value().ok().unwrap(),
        )
        .unwrap();

        let mut dilated = buffer.clone();
        dilate(
            &eroded,
            &mut dilated,
            &get_structuring_element(
                MORPH_RECT,
                Size {
                    width: 4,
                    height: 4,
                },
                Point_ { x: -1, y: -1 },
            )
            .unwrap(),
            Point_ { x: -1, y: -1 },
            1,
            BORDER_CONSTANT,
            morphology_default_border_value().ok().unwrap(),
        )
        .unwrap();

        let mut thresholded = buffer.clone();
        threshold(
            &dilated,
            &mut thresholded,
            channel_threshold,
            255.0,
            THRESH_BINARY,
        )
        .unwrap();
        thresholded
    }

    let blue = process_channel(&split_frame.get(0).unwrap(), 100.0);
    let green = process_channel(&split_frame.get(1).unwrap(), 130.0);
    let red = process_channel(&split_frame.get(2).unwrap(), 150.0);

    let mut combined = Mat::default().unwrap();
    bitwise_or(&blue, &green, &mut combined, &no_array().unwrap()).unwrap();
    let mut track = Mat::default().unwrap();
    bitwise_or(&combined, &red, &mut track, &no_array().unwrap()).unwrap();

    let mut really_blue = Mat::default().unwrap();
    subtract(&blue, &green, &mut really_blue, &no_array().unwrap(), -1)?;

    Ok((track, really_blue, green, red))

    /*save_frame_to_file(
        "captures/debug/blue-0.png",
        &split_frame_processed.get(0).unwrap().0,
    )?;
    save_frame_to_file(
        "captures/debug/blue-1.png",
        &split_frame_processed.get(0).unwrap().1,
    )?;
    save_frame_to_file(
        "captures/debug/green-0.png",
        &split_frame_processed.get(1).unwrap().0,
    )?;
    save_frame_to_file(
        "captures/debug/green-1.png",
        &split_frame_processed.get(1).unwrap().2,
    )?;
    save_frame_to_file(
        "captures/debug/red-0.png",
        &split_frame_processed.get(2).unwrap().0,
    )?;
    save_frame_to_file(
        "captures/debug/red-1.png",
        &split_frame_processed.get(2).unwrap().3,
    )?;*/

    // Ok(split_frame_processed)
}

fn main() {
    run().unwrap()
}
